<?php

namespace Drupal\migrate_orphans\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManager;
use Drupal\migrate_tools\Drush\MigrateToolsCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class MigrateOrphansCommands extends MigrateToolsCommands {

  use StringTranslationTrait;

  /**
   * Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $connection;

  /**
   * MigrateToolsCommands constructor.
   *
   * @param \Drupal\migrate\Plugin\MigrationPluginManager $migrationPluginManager
   *   Migration Plugin Manager service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   Date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValue
   *   Key-value store service.
   */
  public function __construct(
    MigrationPluginManager $migrationPluginManager,
    DateFormatter $dateFormatter,
    EntityTypeManagerInterface $entityTypeManager,
    KeyValueFactoryInterface $keyValue,
    Connection $connection
  ) {
    parent::__construct($migrationPluginManager, $dateFormatter, $entityTypeManager, $keyValue);
    $this->connection = $connection;
  }

  /**
   * Remove imported records that has been removed in origin source
   *
   * @param $migration_name
   *   Name of the migration to check
   * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
   *
   * @option dry-run
   *   Do not delete, just report the records
   * @option group
   *   A comma-separated list of migration groups to list
   * @option output-ids
   *   Outputs a list of source and destination ids of the orphans found.
   * @option limit
   *   The number of records to purge at a time, defaults to 100.
   * @usage drush migrate:orphans:purge migration
   *   Removes orphans from migration
   * @usage drush migrate-orphans-purge --dry-run=1 <migration>
   *   Show the number of orphans that would be purged from a migration.
   *
   * @command migrate:orphans:purge
   * @aliases morp,migrate-orphans-purge,migrate:orphans-purge
   *
   * @throws \Exception
   *  If there are not enough parameters to the command.
   */
  public function orphansPurge($migration_name = '', array $options = ['dry-run' => FALSE, 'delete' => TRUE, 'group' => NULL, 'output-ids' => NULL, 'limit' => 100]) {
    $migrations = $this->migrationsList($migration_name, $options);

    // Default to 100 records at a time.
    $limit = 100;
    if (is_numeric($options['limit'])) {
      if (intval($options['limit']) > 0) {
        $limit = intval($options['limit']);
      }
    }

    if (empty($migrations)) {
      $this->logger()->error(($this->t('No migrations found.')));
    }

    foreach ($migrations as $migration_list) {
      /** @var \Drupal\migrate\Plugin\Migration $migration */
      foreach ($migration_list as $migration) {
        if ($migration->getStatus() != MigrationInterface::STATUS_IDLE) {
          throw new \Exception(dt('Migration not idle, waiting to finish. Status is: ' . $migration->getStatus()));
        }

        $rows = $this->connection->select('migrate_map_' . strtolower($migration->getPluginId()), 'm')
          ->fields('m', ['sourceid1', 'destid1'])
          ->isNotNull('destid1')
          ->execute()
          ->fetchAllKeyed(0, 1);

        $count = count($rows);
        $orphans = $rows;

        $dry_run = !empty($options['dry-run']);

        $this->logger()->notice("Total migrated: " . $count);

        /** @var \Drupal\migrate_plus\Plugin\migrate\source\Url $source */
        $source = $migration->getSourcePlugin();

        if (get_class($source) == 'Drupal\migrate_plus\Plugin\migrate\source\Url' ||
          is_subclass_of(get_class($source), 'Drupal\migrate_plus\Plugin\migrate\source\Url')) {
          // Init first source:
          /** @var \Drupal\migrate_plus\Annotation\DataParser */
          $dataparser = $source->getDataParserPlugin();
          $dataparser->rewind();

          while ($dataparser->valid()) {
            $key = $dataparser->key();
            $id = reset($key);
            unset($orphans[$id]);
            $dataparser->next();
          }
        }
        elseif (is_subclass_of(get_class($source), 'Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase')) {
          $rows = $source->query()->execute();
          $ids = $source->getIds();
          if (count($ids) === 1) {
            $id = array_key_first($ids);
          }
          else {
            $this->logger()->error('Migrate class ' . get_class($source) . ' not compatible (multiple IDs).');
          }
          foreach ($rows as $row) {
            unset($orphans[$row[$id]]);
          }
        }
        else {
          $this->logger()->error('Migrate class ' . get_class($source) . ' not compatible.');
          return;
        }

        /** @var \Drupal\migrate\Plugin\migrate\destination\Entity $destination */
        $destination = $migration->getDestinationPlugin();

        if (!empty($options['output-ids'])) {
          $this->logger()->notice('Migration: ' . $migration->getPluginId());
          if (count($orphans)) {
            $this->logger()->notice('Total orphans: ' . count($orphans));
            $this->output()->writeln('sourceid1, destid1');
            foreach ($orphans as $key => $value) {
              $this->output()->writeln($key . ', ' . $value);
            }
          }
          else {
            $this->logger()->notice('No orphans found');
          }
        }

        if (!empty($orphans)) {
          $deleted = $disabled = FALSE;
          if (!$dry_run && $options['delete']) {
            if (!empty($destination->getPluginDefinition()['id'])) {
              $provider = ltrim(stristr($destination->getPluginDefinition()['id'], ':'), ':');
              $controller = $this->entityTypeManager->getStorage($provider);
              if (count($orphans) > $limit) {
                for ($i = 0; $i <= count($orphans); $i += $limit) {
                  $end = $i + $limit > count($orphans) ? count($orphans) : $i + $limit;
                  $to_delete = array_slice($orphans, $i, $end);
                  $entities = $controller->loadMultiple($to_delete);
                  $controller->delete($entities);
                  $this->logger()->notice('Deleted entites from ' . $i . ' to ' . $end);
                }
              }
              else {
                $entities = $controller->loadMultiple($orphans);
                $controller->delete($entities);
              }
              $deleted = TRUE;
            }
          }
          else {
            if (!$dry_run && !$options['delete']) {
              if (!empty($destination->getPluginDefinition()['id'])) {
                $provider = ltrim(stristr($destination->getPluginDefinition()['id'], ':'), ':');
                $controller = $this->entityTypeManager->getStorage($provider);
                $entities = $controller->loadMultiple($orphans);

                // Unpublish orphans
                foreach ($entities as $node) {
                  $node->set('status', 0);
                  $node->save();
                }
                $disabled = TRUE;
              }
            }
          }

          if ($deleted || $disabled) {
            $items_purged = [];
            foreach ($orphans as $current_id => $orphan) {
              $source_ids = $migration->getSourcePlugin()->getIds();
              $items_purged[$orphan] = $orphan;
              $values = [];
              foreach (array_keys($source_ids) as $source_id) {
                $values[$source_id] = $current_id;
              }
              $migration->getIdMap()->delete($values);
            }
            if ($dry_run) {
              $this->logger()->success(t("There are all @count orphan element(s) to remove.\nDestination IDs: @items", [
                '@count' => count($items_purged),
                '@items' => implode(', ', $items_purged),
              ]));
            }
            else {
              $this->logger()->success(t("Removed all @count orphan element(s).\nDestination IDs: @items", [
                '@count' => count($items_purged),
                '@items' => implode(', ', $items_purged),
              ]));
            }
          }

        }
        else {
          $this->logger()->success('There are no more orphan elements to purge.');
        }
      }
    }
  }

  /**
   * Disable imported records that has been removed in origin source
   *
   * @param $migration_name
   *   Name of the migration to check
   * @param array $options An associative array of options whose values come from cli, aliases, config, etc.
   *
   * @option dry-run
   *   Do not disable, just report the records
   * @option group
   *   A comma-separated list of migration groups to list
   * @usage drush migrate:orphans:disable migration
   *   Disables orphans from migration
   *
   * @command migrate:orphans:disable
   * @aliases mord,migrate-orphans-disable,migrate:orphans-disable
   *
   * @throws \Exception
   *  If there are not enough parameters to the command.
   */
  public function orphansDisable($migration_name = '', array $options = ['dry-run' => FALSE, 'group' => NULL]) {
    $this->orphansPurge($migration_name, [$options['dry-run'], FALSE]);
  }

}
