CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Usage
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

When using migrate to import data from external data sources, we often have the issue, that the external data source deletes records we previously imported. In our system remain "orphans" and our migrate_map tables are full of orphaned entries.

This modules comes with a handy drush command to remove orphans from a specific migration by comparing the migrate_map table and the external data source.

You have two options:
 * Delete orphaned entities
 * Maintainers


REQUIREMENTS
------------

This module requires migrate and migrate_plus.


USAGE
----------------------
```
drush migrate:orphans:purge <migration-name>
```
```
drush migrate:orphans:disable <migration-name>
```

The `--dry-run` option may be passed to see the number of records that would be
purged:

    drush migrate:orphans-purge --dry-run=1 <migration-name>


INSTALLATION
------------

 * Install the  Migrate Orphans Purger module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


MAINTAINERS
-----------

   * ayalon - https://www.drupal.org/u/ayalon


Supporting organizations:

 * Liip AG. - https://www.drupal.org/liip
